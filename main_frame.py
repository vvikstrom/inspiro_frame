# Kivy imports 
import kivy
from kivy.lang import Builder
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.clock import Clock

# Local imports
from inspirobot_api import InspiroBot
from files import FileManager

# Module imports
import logging
from datetime import datetime as dt
from datetime import timedelta as td

kivy.require('1.9.1')
Builder.load_file('layout.kv')


class StandbyLayout(BoxLayout):
    def build(self):
        return self


class StandbyApp(App, StandbyLayout):
    def build(self):
        return self


class MainLayout(BoxLayout):
    def build(self):
        return self


class LocalTimeClock(MainLayout):
    def update_clock(self, *args):
        labelString = \
            dt.now().strftime("%H:%M") + '\n' \
            + dt.now().strftime('%Y-%m-%d') + '\n' \
            + dt.now().strftime('%A') + '\n' \
            + 'Week ' + dt.now().strftime('%W')
        self.ids.label1.text = labelString


class Quote(LocalTimeClock):
    def set_source(self, source=None):
        if source is not None:
            self.ids.quote.source = source
        else:
            ib = InspiroBot()
            fm = FileManager()
            fm.create('quote.jpg', ib.get_img_obj())
            self.ids.quote.source = 'quote.jpg'
        self.ids.quote.reload()

    def update_image(self, *args):
        self.set_source()
        self.ids.quoteButton.disabled = False

    def loading(self):
        self.ids.quoteButton.disabled = True
        self.set_source('loading/image-loading.gif')


class MainApp(App, Quote):
    def build(self):
        return self


class ControlFlow(MainApp):
    def __init__(self, **kwargs):
        self.updateDelayS = 60*60
        self.timerEvent = None
        super(ControlFlow, self).__init__(**kwargs)

    def set_timer(self):
        self.timerEvent = Clock.schedule_interval(
            self.update_image, self.updateDelayS)

    def reset_timer(self):
        logging.info('Resetting timer')
        Clock.unschedule(self.timerEvent)
        self.timerEvent = Clock.schedule_interval(
            self.update_image, self.updateDelayS)
    
    def start_clock(self):
        logging.info('Starting clock')
        Clock.schedule_interval(self.update_clock, 1)
    
    def run_flow(self):
        self.set_source()
        self.set_timer()
        self.start_clock()
        self.run()


class StandbyFlow(StandbyApp):
    def __init__(self, **kwargs):
        super(StandbyFlow, self).__init__(**kwargs)
    
    def run_flow(self):
        self.run()

        
if __name__ == "__main__":
   # frame = ControlFlow()
   # frame.run_flow()
   # del frame
    frame = MainApp()
    frame.run()
