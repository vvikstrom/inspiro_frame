import pyowm
from configparser import ConfigParser

def test():
    config = ConfigParser()
    # config['LOCATION'] = {}
    # config['LOCATION']['country'] = 'se'
    # with open ('OWM_CONFIG_TEST.ini', 'w') as configfile:
    #     config.write(configfile)
    config.read('OWM_CONFIG.ini')
    print(config.sections())
    print(config['LOCATION']['country'])
    
    owm = pyowm.OWM(API_key=config['KEYS']['apiKey'])
    place = config['LOCATION']['city'] + ',' + config['LOCATION']['country']
    observation = owm.weather_at_place(place)
    w = observation.get_weather()
    print(w)
    print(w.get_wind())
    print(w.get_temperature())
    l = observation.get_location()
    print(l.get_lon())
    print(l.get_lat())

if __name__ == "__main__":
    test()
