import os
import shutil
import logging

class FileManager:
    
    def create(self, file_name, data):
        logging.info('Writing to file ' + file_name)
        with open(file_name, 'wb') as f:
            #for chunk in data:
            f.write(data)
        return file_name
    
    def create_directory(self, dir_path):
        if not os.path.isdir(dir_path):
            os.mkdir(dir_path)
        return dir_path

    def clear_directory(self, dir_path):
        if not os.path.isdir(dir_path):
            return False
        else:
            shutil.rmtree(dir_path)
            return True
    
    def remove(self, file_path):
        return os.remove(file_path)
