import requests
import logging

class InspiroBot:
    
    def __init__(self):
        self._api_url = 'http://inspirobot.me/api?generate=true'
    
    def make_get_request(self, url):
        logging.info('Requesting from: ' + url)
        r = requests.get(url)
        return r.status_code, r

    def get_api_url(self):
        return self._api_url

    def get_img_url(self):
        status, r = self.make_get_request(self._api_url)
        if status == 200:
            url = r.text
            logging.info('img_url: ' + url)
            return url
        else:
            return None
    
    def get_img_obj(self):
        url = self.get_img_url()
        status, r = self.make_get_request(url)
        if status == 200:
            logging.info('Positive response')
            return r.content
        else:
            logging.info(status)
            return None
    
